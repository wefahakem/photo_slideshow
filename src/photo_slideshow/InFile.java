package photo_slideshow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class InFile {

	private String filename;
	private int rows = 0;
	public String[][] contentH;
	public String[][] contentV;

	public InFile(String filename) {
		super();
		this.filename = filename;
	}

	public void inFileTrait() throws NumberFormatException, IOException {

		File file = new File(filename);

		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = br.readLine();

		this.rows = Integer.valueOf(line);

		int i = 0;
		int kH = 0;
		int kV = 0;
		this.contentH = new String[this.rows][3];
		this.contentV = new String[this.rows][3];

		while ((line = br.readLine()) != null) {

			char type = line.charAt(0);
			

			if (type == 'H') {
				this.contentH[kH][0] = String.valueOf(i);
				this.contentH[kH][1] = Character.toString(line.charAt(2));
				this.contentH[kH][2] = line.substring(4, line.length());

				kH++;

			} else if (type == 'V') {
				this.contentH[kV][0] = String.valueOf(i);
				this.contentH[kV][1] = Character.toString(line.charAt(2));
				this.contentH[kV][1] =  line.substring(4, line.length());
				kV++;

			}
			i++;
		}
		br.close();
	}

	public void debug() {
		// System.out.println(this.contentH.length);
		// System.out.println(this.contentV.length);

	}
}
